<?php

//input:string
//Return: string

$str = "Intorducing to Iftekhar'127301";

// Outputs: Intorducing to Iftekhar\'127301
//Returns a string with backslashes before characters that need to be escaped. 
//These characters are single quote ('), double quote ("), backslash (\) and NUL (the NULL byte).
echo addslashes($str);
?>