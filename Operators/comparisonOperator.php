<?php
$x = 45;  
$y = 6.5;

var_export($x === $y); // Identical; returns false because types are not equal
echo '<hr>';
var_export($x == $y); //Equal; returns true because values are equal
echo '<hr>';
var_export($x != $y); //Not Equal; returns false because values are equal
echo '<hr>';
var_export($x !== $y);//Not identical;Returns true if $x is not equal to $y, or they are not of the same type
echo '<hr>';
var_export($x >= $y); //Greater than or equal to; returns true because $x is greater than or equal to $y
echo '<hr>';
var_export($x <= $y); //Less than or equal to; returns true because $x is greater than or equal to $y
?> 